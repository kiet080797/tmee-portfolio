import React from 'react'
import Image from 'next/image'
import Portfolio_1 from '@/public/img/portfolio-1.png'
import Portfolio_2 from '@/public/img/portfolio-2.png'
import Portfolio_3 from '@/public/img/portfolio-3.png'
import './style.css'
import '@/Responsive/responsive.css'
import { HREF } from '@/utils/contants'

const PortfolioPage = () => {
  return (
    <div>
        <section className="portfolio section" id="portfolio">
          <div className="container">
            <div className="row">
              <div className="section-title padding-15">
                <h2>Portfolio</h2>
              </div>
            </div>
            <div className="row">
              <div className="portfolio-heading padding-15">
                <h2>My Works :</h2>
              </div>
            </div>
            <div className="row">
              <div className="portfolio-item padding-15">
                <div className="portfolio-item-inner shadow-dark">
                  <div className="portfolio-img">
                    <Image src={Portfolio_1} className="img" alt="" ></Image>
                  </div>
                </div>
              </div>
              <div className="portfolio-item padding-15">
                <div className="portfolio-item-inner shadow-dark">
                  <div className="portfolio-img">
                    <Image src={Portfolio_2} className="img" alt="" ></Image>
                  </div>
                </div>
              </div>
              <div className="portfolio-item padding-15">
                <div className="portfolio-item-inner shadow-dark">
                  <div className="portfolio-img">
                    <Image src={Portfolio_3}  className="img" alt="" ></Image>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>
  )
}

export default PortfolioPage
