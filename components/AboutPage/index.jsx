import React from 'react'
import Link from 'next/link'
import './style.css'
import '@/Responsive/responsive.css'
import { HREF } from '@/utils/contants'

const AboutPage = ({onClickSidebar}) => {

  return (
    <div>
        <section className="about section" id="about">
          <div className="container">
            <div className="row">
              <div className="section-title padding-15">
                <h2>About Me</h2>
              </div>
            </div>
            <div className="row">
              <div className="about-content padding-15">
                <div className="row">
                  <div className="about-text padding-15">
                    <h3>I'm My Luong and <span>Passionate Teacher</span></h3>
                  </div>
                </div>
                <div className="row">
                  <div className="personal-info padding-15">
                    <div className="row">
                      <div className="info-item padding-15">
                        <p>Birthday : <span>23 November 2003</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Age : <span>21</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Facebook : <span>thuymy2311</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Email : <span>luongthithuymy2311@gmail.com</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Degree : <span>International Relations</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Phone : <span>+84 964 854 892</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>City : <span>Ho Chi Minh</span></p>
                      </div>
                      <div className="info-item padding-15">
                        <p>Freelance : <span>Available</span></p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="buttons padding-15">
                        <Link href="/" className="btn">Download CV</Link>
                        <Link href="#contact" className="btn" onClick={()=> onClickSidebar(HREF.CONTACT)}>Contact Me</Link>
                      </div>
                    </div>
                  </div>
                  <div className="skills padding-15">
                    <div className="row">
                      <div className="skill-item padding-15">
                        <h5>Communication</h5>
                        <div className="progress">
                          <div className="progress-in" style={{ width: "91%" }}></div>
                          <div className="skill-percent">91%</div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="skill-item padding-15">
                        <h5>Self-discipline</h5>
                        <div className="progress">
                          <div className="progress-in" style={{ width: "76%" }}></div>
                          <div className="skill-percent">76%</div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="skill-item padding-15">
                        <h5>Collaboration</h5>
                        <div className="progress">
                          <div className="progress-in" style={{ width: "86%" }}></div>
                          <div className="skill-percent">86%</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="education padding-15">
                    <h3 className="title">Education</h3>
                    <div className="row">
                      <div className="timeline-box padding-15">
                        <div className="timeline shadow-dark">
                          <div className="timeline-item">
                            <div className="circle-dot"></div>
                            <h3 className="timeline-date">
                              <i className="fa fa-calendar"></i>2021 - 2025
                            </h3>
                            <h4 className="timeline-title">University of Social Science and Humanities (USSH)</h4>
                            <div className="timeline-major"> <i>Major: International Relations</i></div>
                            <p className="timeline-text">
                            The University of Social Sciences and Humanities, 
                            Vietnam National University Ho Chi Minh City 
                            (VNUHCM-University of Social Sciences and Humanities) is a member of the Vietnam National University Ho Chi Minh City system. 
                            The university was established in 1957, formerly known as the Van Khoa (under Saigon University Institute), 
                            University of Tong Hop Ho Chi Minh City. 
                            It is a leading research and training center in the fields of social sciences and humanities in the southern region.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="experience padding-15">
                    <h3 className="title">Experience</h3>
                    <div className="row">
                      <div className="timeline-box padding-15">
                        <div className="timeline shadow-dark">
                          <div className="timeline-item">
                            <div className="circle-dot"></div>
                            <h3 className="timeline-date">
                              <i className="fa fa-calendar"></i>2021 - 2022
                            </h3>
                            <h4 className="timeline-title">EDUPIA CORPORATION CENTER</h4>
                            <div className="timeline-major"> <i>Tutor</i></div>
                            <div>
                              <h4 className="timeline-text-title">Responsibilities:</h4>
                              <ul className="timeline-text-list">
                                <li>Online English Tutor for young learners</li>
                              </ul>
                            </div>
                          </div>

                          <div className="timeline-item">
                            <div className="circle-dot"></div>
                            <h3 className="timeline-date">
                              <i className="fa fa-calendar"></i>July 2022 - February 2024
                            </h3>
                            <h4 className="timeline-title">EDUTALK CENTER</h4>
                            <div className="timeline-major"> <i>Teaching Assistant & Admin Collaborator</i></div>
                            <div>
                              <h4 className="timeline-text-title">Responsibilities:</h4>
                              <ul className="timeline-text-list">
                                <li>Assessing students' English level for class classification</li>
                                <li>Tutoring students, monitoring the exam</li>
                                <li>Attending training classes, conferences, or faculty meetings</li>
                              </ul>
                            </div>
                          </div>

                          <div className="timeline-item">
                            <div className="circle-dot"></div>
                            <h3 className="timeline-date">
                              <i className="fa fa-calendar"></i>November 2023 - Present
                            </h3>
                            <h4 className="timeline-title">MS.DUYEN EHOUSE CENTER</h4>
                            <div className="timeline-major"> <i>Teacher</i></div>
                            <div>
                              <h4 className="timeline-text-title">Responsibilities:</h4>
                              <ul className="timeline-text-list">
                                <li>Teaching General English, TOEIC and English in Communication for classes (Size: 20-40 students)</li>
                                <li>Preparing teaching materials and making teaching video when required</li>
                                <li>Attending training classes, conferences, or faculty meetings</li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>
  )
}

export default AboutPage
