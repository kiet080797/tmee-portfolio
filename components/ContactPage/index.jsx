import React from 'react'
import './style.css'
import '@/Responsive/responsive.css'
import { HREF } from '@/utils/contants'

const ContactPage = () => {
  return (
    <div>
        <section className="contact section" id="contact">
          <div className="container">
            <div className="row">
              <div className="section-title padding-15">
                <h2>Contact Me</h2>
              </div>
            </div>
            <h3 className="contact-title padding-15">Do Have You Any Questions ?</h3>
            <h4 className="contact-sub-title padding-15">I'M AT YOUR SERVICES</h4>
            <div className="row">
              <div className="contact-info-item padding-15">
                <div className="icon"><i className="fa fa-phone"></i></div>
                <h4>Call Me On</h4>
                <p>+84 964 854 892</p>
              </div>
              <div className="contact-info-item padding-15">
                <div className="icon"><i className="fa fa-map-marker-alt"></i></div>
                <h4>Office</h4>
                <p>Ho Chi Minh City</p>
              </div>
              <div className="contact-info-item padding-15">
                <div className="icon"><i className="fa fa-envelope"></i></div>
                <h4>Email</h4>
                <p>luongthithuymy2311@gmail.com</p>
              </div>
              <div className="contact-info-item padding-15">
                <div className="icon"><i className="fa fa-globe-europe"></i></div>
                <h4>Facebook</h4>
                <p>thuymy2311</p>
              </div>
            </div>
            <h3 className="contact-title padding-15">SEND ME AN EMAIL</h3>
            <h4 className="contact-sub-title padding-15">I'M VERY RESPONSIVE TO MESSAGES</h4>
            <div className="row">
              <div className="contact-form padding-15">
                <div className="row">
                  <div className="form-item col-6 padding-15">
                    <div className="form-group">
                      <input type="text" className="form-control" placeholder="Name"/>
                    </div>
                  </div>
                  <div className="form-item col-6 padding-15">
                    <div className="form-group">
                      <input type="email" className="form-control" placeholder="Email"/>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="form-item col-12 padding-15">
                    <div className="form-group">
                    <input type="text" className="form-control" placeholder="Subject"/>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="form-item col-12 padding-15">
                    <div className="form-group">
                     <textarea name="" className="form-control" id="" placeholder="Message"></textarea>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="form-item col-12 padding-15">
                      <button type="submit" className="btn">Send Message</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    </div>
  )
}

export default ContactPage
