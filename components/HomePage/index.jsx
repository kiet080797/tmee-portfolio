import React, { useEffect } from 'react'
import Link from 'next/link'
import Image from 'next/image'
import Avatar from '@/public/img/avatar.jpeg'
import Typed from 'typed.js';
import './style.css'
import '@/Responsive/responsive.css'
import { HREF } from '@/utils/contants'

const HomePage = ({onClickSidebar}) => {

  useEffect(() => {
    const typed = new Typed(".typing", {
      strings: ["Passionate Teacher", "Fresher", "Third-year College Student"],
      typeSpeed: 100,
      backSpeed: 60,
      loop: true
    });

    return () => {
      typed.destroy();
    };
  }, []);

  return (
    <div>
        <section className="home section active" id="home">
          <div className="container">
            <div className="row">
              <div className="home-info padding-15">
                <h3 className="hello">Hello, my name is  <span className="name">My Luong</span></h3>
                <h3 className="my-profession">I'm a <span className="typing"></span></h3>
                <p>
                  I'm a passionate teacher. I have considerable
                  experience as an energetic tutor for numerous
                  students. I'm looking for a stimulating position to
                  help me develop my mentoring and teaching
                  skills further. 
                </p>
                <Link href='#about' className="btn" onClick={()=> onClickSidebar(HREF.ABOUT)}>More About Me</Link>
              </div>
              <div className="home-img padding-15">
               <Image src={Avatar} className="img" height={400} alt=""/>
              </div>
            </div>
          </div>
        </section>
    </div>
  )
}


export default HomePage
