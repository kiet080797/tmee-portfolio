import React from 'react'
import './style.css'
import '@/Responsive/responsive.css'

const ServicePage = () => {
  return (
    <div>
        <section className="service section" id="service">
          <div className="container">
            <div className="row">
              <div className="section-title padding-15">
                <h2>Skills</h2>
              </div>
            </div>
            <div className="row">
              <div className="service-item padding-15">
                  <div className="service-item-inner">
                    <div className="icon">
                      <div className="fa fa-graduation-cap"></div>
                    </div>
                    <h4>Teaching</h4>
                    <p>It is the abilities and methods that educators use to impart knowledge and create a positive learning environment</p>
                  </div>
              </div>
              <div className="service-item padding-15">
                <div className="service-item-inner">
                  <div className="icon">
                    <div className="fa fa-laptop-code"></div>
                  </div>
                  <h4>Lesson Planning</h4>
                  <p>Designing and organizing lessons, preparing appropriate teaching materials, and resources tailored to learning objectives and student needs</p>
                </div>
              </div>
              <div className="service-item padding-15">
                <div className="service-item-inner">
                  <div className="icon">
                    <div className="fa fa-person-circle-question"></div>
                  </div>
                  <h4>Classroom Management</h4>
                  <p>Time management, managing student behavior, and handling challenging situations in the classroom</p>
                </div>
              </div>
              <div className="service-item padding-15">
                <div className="service-item-inner">
                  <div className="icon">
                    <div className="fa fa-head-side-cough"></div>
                  </div>
                  <h4>Presentation</h4>
                  <p>It is the abilities and techniques used to effectively deliver information or ideas to an audience in a clear, engaging, and impactful manner</p>
                </div>
              </div>
              <div className="service-item padding-15">
                <div className="service-item-inner">
                  <div className="icon">
                    <div className="fa fa-search"></div>
                  </div>
                  <h4>Research Skill</h4>
                  <p>It is the ability to gather information, analyze data, and logically infer from various sources to achieve a specific goal</p>
                </div>
              </div>
              <div className="service-item padding-15">
              <div className="service-item-inner">
                <div className="icon">
                  <div className="fa fa-bullhorn"></div>
                </div>
                <h4>Interpersonal Skill</h4>
                <p>It is the ability to communicate and interact effectively with others in a variety of situations and environments</p>
              </div>
             </div>
            </div>
          </div>
        </section>
    </div>
  )
}

export default ServicePage
