/** @type {import('next').NextConfig} */
const nextConfig = {
  head: {
    title: 'Trang của tôi',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'description', content: 'Mô tả của trang' },
    ],
    link: [{ rel: 'icon', href: '/favicon.ico' }],
  },
};

export default nextConfig;
