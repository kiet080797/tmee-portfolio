import { Inter } from "next/font/google";
import "./globals.css";
import '@fortawesome/fontawesome-free/css/all.min.css';
import { Metadata } from 'next'

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Tmee's portfolio",
  description: "I'm a passionate teacher. I have considerable experience as an energetic tutor for numerous students. I'm looking for a stimulating position to help me develop my mentoring and teaching skills further.",
  icons: {
    icon: "/favicon.ico",
    shortcut: "/favicon.ico",
  },
  openGraph: {
    title: "Tmee's portfolio",
    description: "I'm a passionate teacher. I have considerable experience as an energetic tutor for numerous students. I'm looking for a stimulating position to help me develop my mentoring and teaching skills further.",
    images: [
      {
        url: "https://scontent.fsgn5-9.fna.fbcdn.net/v/t39.30808-6/425718850_3655694158038260_3206747365276970920_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=5f2048&_nc_ohc=3tRupz7A-rwQ7kNvgGv4Zuu&_nc_ht=scontent.fsgn5-9.fna&oh=00_AfD_y_oAQGqrozbg7ZLH8RCgwq7tC1vtq6CH611bnhaxEQ&oe=663FED2E",
        height: 600,
        alt: 'My avatar',
      },
    ],
  },
}

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
  );
}
